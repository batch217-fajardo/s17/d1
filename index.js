// console.log("Hello World!");

// Functions (Function Declaration)
function printName() {
	console.log("My name is John");
}
printName();

function declaredFunction() {
	console.log("Hello World from declaredFunction")
}
declaredFunction();

 // Function Expression
let variableFunction = function(){
	console.log("Hello Again!");
}
variableFunction();

let funcExpression = function funcName(){
	console.log("Hello from the other side");
}
funcExpression();

funcExpression = function(){
	console.log("Updated funcExpression");
}
funcExpression();

// Re-assigning declaredFunction() value;

declaredFunction = function(){
	console.log("Updated declaredFunction");
}
declaredFunction();

// Re-assigning function declared with const

const constantFunc = function(){
	console.log("Initialized with const!");
}
constantFunc();

// Re-assignment of a const function
/*constantFunc = function() {
	console.log("Can we re-assign it?");
}
constantFunc();*/

// Function Scoping

{
	let localVar = "Armando Perez";
	console.log(localVar);
}
// console.log(localVar); --> inaccessible

let globalScope = "Mr. Worldwide";
console.log(globalScope);

// Function Scoping

function showNames(){
	// Function scoped variables:
	const functionConst = "John";
	let functionLet = "Jane";
	console.log(functionConst);
	console.log(functionLet);
}
showNames();

// console.log(functionConst);
// 	console.log(functionLet);
// --> cannot be used because these variables are located inside the scope of a function

// Nested Function
function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}
	nestedFunction();
}
myNewFunction();
// nestedFunction(); --> will cause error because the funtion is located to a function scoped parent function.

//Function and Global Scope Variable
let globalName = "Alexandro"

function myNewFunction2(){
	let nameInside = "Renz";

console.log(globalName);
}
myNewFunction2();
// console.log(nameInside);


// Using alert()
alert("Hello World");

function showAlert(){
	alert("Hello user!");
}
showAlert();
console.log("I will only log in the console when alert is dismissed.");

// Using prompt - gathers user input.
let samplePrompt = prompt("Enter your Name.");

console.log("Hello, " + samplePrompt + ".");
console.log(typeof samplePrompt);

function printWelcomeMessage (){
	let firstName = prompt("Enter your first name.")
	let lastName = prompt("Enter your last name.")

	console.log("Hello, " + firstName + " " + lastName);
	console.log("Welcome to my page!");
}
printWelcomeMessage();

// Naming functions
function getCourse(){
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}
getCourse();

// Avoid generic names
function get(){
	let name = "Jamie";
	console.log(name);
}
get();

// Avoid pointless and inappropriate names
function foo(){
	console.log(25%5);
}
foo ();

// Follow camelCase --> myNameIsErica
function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1, 500, 000");
}
displayCarInfo();

// other naming conventions:
// snake_case --> my_name_is_erica
// kebab-case --> my-name-is-erica